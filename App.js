import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import * as Location from 'expo-location';

export default function App() {
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);
  const [data, setData] = useState(null);

  let apiKey = 'f054ead9ceccb4b93a7607fca771aea3';

  const getCity = async () => {
    fetch(`https://api.openweathermap.org/data/2.5/weather?lat=${location.coords.latitude}&lon=${location.coords.longitude}&appid=${apiKey}&units=metric&lang=fr`)
        .then(response => response.json())
        .then(data => {
          setData(data);
        });
  }

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        setErrorMsg('Permission to access location was denied');
        return;
      }
      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);
  
  useEffect(() => {
    location && getCity();
  }, [location]);

  return (
    <View style={styles.container}>
      {/* Attendre que data soit charger avant d'afficher */}
      {data && console.log('Icon :' + data.weather[0].icon)}
      {data && 
      ( 
        <>
          <Text style={styles.paragraph}>Votre ville est : {data.name}</Text>
          <Text style={styles.paragraph}>La température extérieure est de : {data.main.temp}°C</Text>
          <Text style={styles.paragraph}>Le temps actuel est : {data.weather[0].description}</Text>
          <Image style={styles.image} source={{
            uri: `http://openweathermap.org/img/w/${data.weather[0].icon}.png`
          }}/>
        </>
      )}
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image:{
    width: 50,
    height: 50,
  },
});
